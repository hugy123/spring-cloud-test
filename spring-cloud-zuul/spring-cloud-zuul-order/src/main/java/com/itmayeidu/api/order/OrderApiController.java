package com.itmayeidu.api.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableEurekaClient
@SpringBootApplication
public class OrderApiController {

	@RequestMapping("/")
	public String index() {
		return "我是订单服务项目";
	}

	public static void main(String[] args) {
		SpringApplication.run(OrderApiController.class, args);
	}

}
