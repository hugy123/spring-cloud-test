package com.hugy.feign.order;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

// name 指定服务名称
@FeignClient(name = "app-itmayiedu-member")
public interface MemberApifeign {

    @RequestMapping("/getMember")
    public String getMember();
}
