package com.hugy.feign.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class OrderController {

	@Autowired
	private MemberApifeign memberApifeign;

	@RequestMapping("/getOrder")
	public String getOrder() {
		String result = memberApifeign.getMember();
		System.out.println("会员服务调用订单服务,result:" + result);
		return result;
	}
}
