package com.hugy.feign.order;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberController {

    @Value("${server.port}")
    public String servicePort;

    @RequestMapping("/getMember")
    public String getMember() {
        return "this is getMember"+servicePort;
    }
}
