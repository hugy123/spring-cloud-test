package com.hugy.zookeeper;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
public class AppOrder {
    public static void main(String[] args) {
        SpringApplication.run(AppOrder.class, args);
    }

    @Bean
//    @LoadBalanced  // 负载均衡  必须使用注册中心上的服务名 不能用ip或域名
    @ConditionalOnMissingBean({RestTemplate.class})
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
