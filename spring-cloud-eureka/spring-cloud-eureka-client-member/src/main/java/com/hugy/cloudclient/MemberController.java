package com.hugy.cloudclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class MemberController {

    @Value("${server.port}")
    public String servicePort;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/getMember")
    public String getMember() {
        return "this is getMember"+servicePort;
    }
}
