package com.hugy.eureka.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class ExtController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private DiscoveryClient discoveryClient;

    private Integer reqCount = 1;

    // 订单服务调用会员服务
    @RequestMapping("/getOrder")
    public String getOrder() {
        // 有两种方式，一种是采用服务别名方式调用，另一种是直接调用 使用别名去注册中心上获取对应的服务调用地址
        String memberUrl = getServiceUrl() + "/getMember";
//        String memberUrl = "http://zk-member/getMember";
        String result = restTemplate.getForObject(memberUrl, String.class);
        System.out.println("订单服务调用会员服务result:" + result);
        return result;
    }

    private String getServiceUrl() {
        List<ServiceInstance> list = discoveryClient.getInstances("app-itmayiedu-member");
        if(list == null || list.size()< 1){
            return null;
        }
        int size = list.size();
        int index = reqCount % size;
        reqCount++;
        return list.get(index).getUri().toString();
    }
}
