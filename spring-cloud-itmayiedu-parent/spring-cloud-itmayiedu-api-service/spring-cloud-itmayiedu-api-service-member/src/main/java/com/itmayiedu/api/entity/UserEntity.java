/**
 * 功能说明:
 * 功能作者:
 * 创建日期:
 * 版权归属:每特教育|蚂蚁课堂所有 www.itmayiedu.com
 */
package com.itmayiedu.api.entity;

import lombok.Data;

/**
 * 功能说明: <br>
 * 创建作者:每特教育-余胜军<br>
 * 创建时间:2018年9月6日 下午10:19:13<br>
 * 教育机构:每特教育|蚂蚁课堂<br>


 * 联系方式:qq644064779<br>

 */
@Data
public class UserEntity {

	private String name;
	private Integer age;

}
