spring-cloud-itmayiedu-parent
    -spring-cloud-itmayiedu-api-service // 纯接口文件
    -spring-cloud-itmayiedu-common      // 存放通用返回 参数
    -spring-cloud-itmayiedu-member-impl // 会员实现类，有Feign客户端 和 Eureka 客户端
    -spring-cloud-itmayiedu-order-impl  // 订单实现类，有Feign客户端 和 Eureka 客户端
